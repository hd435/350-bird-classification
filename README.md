# 350 Bird Classification 

A pytorch implementation of bird classification using resnet 


## Installation
install CUDA v11.3 and corresponding cudnn (https://developer.nvidia.com/rdp/cudnn-archive)

numpy
pandas
os
PIL
cv2
torch
timm
torchvision
tqdm
collections
matplotlib
sklearn
albumentations


## Usage
training: run bird.py 
demonstration: run run.py



