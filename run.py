import numpy as np
import pandas as pd
import os
from PIL import Image
import cv2
import math

import torch
import torchvision
import timm
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm
from collections import defaultdict
import matplotlib.pyplot as plt
import random

from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.model_selection import train_test_split, cross_validate, StratifiedKFold, cross_val_score

# Metric
from sklearn.metrics import f1_score, accuracy_score

# Augmentation
import albumentations
from albumentations.pytorch.transforms import ToTensorV2


#check gpu availability
if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

print(f'Using device: {device}')


#read data, set up labels
root = os.getcwd() 
bird_dir = os.path.join(root, 'birds')

 
model_path = os.path.join(bird_dir, 'checkpoints/birdnet.pth')

labels_file_path = bird_dir + '/birds latin names.csv'
df = pd.read_csv(labels_file_path)
labels = df['class']    #read all labels from birds latin names.csv
index = df['class_index']
class_length = len(labels)


#encode labels
le = LabelEncoder()
le.fit(df['class'])
df['class'] = le.transform(df['class'])
label_map = dict(zip(le.classes_, le.transform(le.classes_)))    #{'name1':1, ......}
label_inv_map = {v: k for k, v in label_map.items()}    #{'name1':1, ......}

train_list_path = []
test_list_path = []
for label in labels:
    if type(label) == str:
        label_code = label_map[label]
        train_path = bird_dir + '/train/' + label 
        test_path = bird_dir + '/test/' + label 
        no = len([name for name in os.listdir(train_path)])
        for i in range(1, no+1):
            if i >= 100:
                i = str(i)
            elif i>=10:
                i = '0' +str(i)
            else:
                i = '00' + str(i)
            train_list_path.append([label_code, train_path + '/' + i + '.jpg'])
        for j in range(1,6):
            j = '00' + str(i)
            test_list_path.append([label_code, test_path + '/' + j + '.jpg'])
df = pd.DataFrame(train_list_path)
sub_df = pd.DataFrame(test_list_path)
df.columns = ['class','image']
sub_df.columns = ['class','image']

label_map = dict(zip(labels, index))
inv_label_map = {index:label for label, index in label_map.items()}





#configure parameters
params = {
    'model': 'seresnext50_32x4d',
    # 'model': 'resnet50d',
    'device': device,
    'lr': 1e-3,
    'batch_size': 1,
    'num_workers': 0,
    'epochs': 50,
    'out_features': df['class'].nunique(),
    'weight_decay': 1e-5
}


transform =  albumentations.Compose(
        [
            albumentations.Resize(150, 150),
            albumentations.Normalize(
                [0.485, 0.456, 0.406], [0.229, 0.224, 0.225],
                max_pixel_value=255.0, always_apply=True
            ),
            ToTensorV2(p=1.0)
        ]
    )


class BirdNet(nn.Module):
    def __init__(self, model_name=params['model'], out_features=params['out_features'],
                 pretrained=True):
        super().__init__()
        self.model = timm.create_model(model_name, pretrained=pretrained)
        n_features = self.model.fc.in_features
        self.model.fc = nn.Linear(n_features, out_features)

    def forward(self, x):
        x = self.model(x)
        return x


model = BirdNet()
model = nn.DataParallel(model)
model = model.to(params['device'])
init = torch.load(model_path)
model.load_state_dict(init)
model.eval()
model.cuda()

def run(model, image_path, label_map):
    #image = image.to(torch.device('cuda'), non_blocking=True)
    untrans_image = cv2.imread(image_path)
    untrans_image = cv2.resize(untrans_image, (150, 150))
    untrans_image = cv2.cvtColor(untrans_image, cv2.COLOR_BGR2RGB)
    image = transform(image=untrans_image)["image"].float()
    image = torch.unsqueeze(image, 0)
    if image.shape == torch.Size([1,150,150,3]):
        image = image.permute(0, 3, 1, 2)  
    image = image.to(torch.device('cuda'), non_blocking=True)
    output = model(image)
    index = torch.argmax(output, dim=1).tolist()[0]
    print(index)
    output = label_map[index]
    return output 



class_label, photo_label = 14, '1.jpg'
test_class = labels[class_label]
image_path = os.path.join(root, 'birds/test', test_class, photo_label)
image = cv2.imread(image_path)

output = run(model, image_path, inv_label_map)
print(output, ', true class: ', test_class)
plt.imshow(image)
text = 'Actual class: ' + test_class + '\nDetected class: ' + output
plt.text(1.5, 1.5, text, fontsize = 'x-large', fontstyle = 'oblique', color = 'red')
plt.show()
